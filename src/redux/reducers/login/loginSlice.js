import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { auth } from '../../../services/firebase';

export const loginSlice = createSlice({
  name: 'login',
  initialState: {
    email: '',
    password: '',
    isLoggedIn: false,
    error: null
  },
  reducers: {
    setEmail: (state, action) => {
      state.email = action.payload;
    },
    setPassword: (state, action) => {
      state.password = action.payload;
    },
    setLoggedIn: (state, action) => {
      state.isLoggedIn = action.payload;
    },
    setError: (state, action) => {
      state.error = action.payload;
    }
  }
});

export const signIn = createAsyncThunk('login/signIn', async (userData, thunkAPI) => {
  try {
    const userCredential = await signInWithEmailAndPassword(auth, userData.email, userData.password);
    const token = await userCredential.user.getIdToken();
    localStorage.setItem('token', token);
    return { email: userData.email, isLoggedIn: true };
  } catch (error) {
    return thunkAPI.rejectWithValue(error.message);
  }
});

export const {
  setEmail, setPassword, setLoggedIn, setError
} = loginSlice.actions;

export default loginSlice.reducer;
