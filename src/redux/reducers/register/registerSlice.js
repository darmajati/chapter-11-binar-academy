import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
// firebase
import { createUserWithEmailAndPassword } from 'firebase/auth';
import { getStorage, ref, uploadBytes } from 'firebase/storage';
import { setDoc, doc } from 'firebase/firestore';
import { db, auth } from '../../../services/firebase';

export const uploadProfileImage = createAsyncThunk(
  'register/uploadProfileImage',
  async (imageData, thunkAPI) => {
    try {
      /* UPLOAD PROFILE IMAGE */
      let file = imageData.fileImage;

      if (file === undefined) file = { name: 'asdfg' };

      const storage = getStorage();
      const storageRef = ref(storage, `images/${file.name}`);

      /* UPLOAD IMAGE TO STORAGE */
      await uploadBytes(storageRef, file)
        .then(() => {
          // no action here
        }).catch((error) => error);
    } catch (error) {
      thunkAPI.rejectWithValue(error.message);
    }
  }
);

export const signUp = createAsyncThunk(
  'register/signup',
  async (userData, thunkAPI) => {
    try {
      /* CREATE NEW USER */
      await createUserWithEmailAndPassword(auth, userData.email, userData.password)
        .then((userCredential) => {
          // signed in
          const userId = userCredential.user;

          // store data to firestore
          try {
            setDoc(doc(db, 'users', userId.uid), {
              email: userData.email,
              fullname: userData.fullname,
              photoURL: userData.photoURL
            });

            // set token to local storage
            userCredential.user.getIdToken()
              .then((token) => localStorage.setItem('token', token));

            return true;
          } catch (error) {
            return error;
          }
        }).catch((error) => {
          thunkAPI.rejectWithValue(error.message);
        });
    } catch (error) {
      thunkAPI.rejectWithValue(error.message);
    }
  }
);

export const registerSlice = createSlice({
  name: 'register',
  initialState: {
    email: '',
    fullname: '',
    profileImage: '',
    isSuccess: false,
    errors: null,
    rawImage: null
  },
  reducers: {
    saveData: (state, action) => {
      state.email = action.payload.email;
      state.fullname = action.payload.fullname;
      state.profileImage = action.payload.profileImage;
    }
  },
  extraReducers: {

  }
});

export const { saveData } = registerSlice.actions;

export const selectData = (state) => state.register;

export default registerSlice.reducer;
